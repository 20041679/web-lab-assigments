// Prepara la carne e specifica come cucinarla
function getBeef(cottura, cb){
    let beef = {howcooked: cottura}; 
    console.log(`Preparando la carne`);
    setTimeout(()=>{
        cb(beef);
    }, 1000);
}

// Cucina la carne secondo il modo di cottura specificato
function cookBeef(beef, cb){
    let cookedBeef = {cooked: beef.howcooked} 
    console.log(`Cucinando la carne`)
    setTimeout(()=>{
        cb(cookedBeef);
    }, 1000);
}

// Prepara i panini e specifica quale condimento usare
function getBuns(condimento, cb){
    let buns = {topping: condimento}; 
    console.log(`Preparando il pane`);
    setTimeout(()=>{
        cb(buns);
    }, 1000);
}

// Metti la carne cucinata tra due panini
function putBeefBetweenBuns(buns,cookedBeef, cb) {    
   let burger = {beef: cookedBeef, buns: buns }; 
   console.log(`Inserendo carne nel pane`);
   setTimeout(()=>{
        cb(burger);
   }, 1000);
}

// Servi l'hamburger con le specifiche della carne e del condimento
function serve(burger) {
    console.log(`Ho preparato un un panino con dentro un hamburger `+
    `${burger.beef.cooked} e condito con ${burger.buns.topping} `); 
}

// makeBurger() prepara l'hamburger in modalità sincrona
const makeBurger = () => {
    getBeef('mediamente cotto', (beef) => {
        cookBeef(beef, (cookedBeef) => {
            getBuns('cipolla', (buns) => {
                putBeefBetweenBuns(buns, cookedBeef, (burger) => {
                    serve(burger);
                })
            })
        })
    });
};

const burger = makeBurger(); // Prepara l'hamburger
