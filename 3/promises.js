// Prepara la carne e specifica come cucinarla
function getBeef(cottura){
    let beef = {howcooked: cottura}; 
    console.log(`Preparando la carne`);
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(beef);
        }, 1000);
    });
}

// Cucina la carne secondo il modo di cottura specificato
function cookBeef(beef){
    let cookedBeef = {cooked: beef.howcooked};
    console.log(`Cucinando la carne`)
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(cookedBeef);
        }, 1000);
    });
}

// Prepara i panini e specifica quale condimento usare
function getBuns(condimento){
    let buns = {topping: condimento}; 
    console.log(`Preparando il pane`);
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(buns);
        }, 1000);
    });
}

// Metti la carne cucinata tra due panini
function putBeefBetweenBuns(buns,cookedBeef) {    
    let burger = {beef: cookedBeef, buns: buns }; 
    console.log(`Inserendo carne nel pane`);
    return new Promise((resolve, reject) => {
         setTimeout(() => {
             resolve(burger);
         }, 1000);
    });
}

// Servi l'hamburger con le specifiche della carne e del condimento
function serve(burger) {
    console.log(`Ho preparato un un panino con dentro un hamburger `+
    `${burger.beef.cooked} e condito con ${burger.buns.topping} `); 
}

// makeBurger() prepara l'hamburger in modalità sincrona
const makeBurger = () => {
    getBeef('mediamente cotto')
        .then((beef) => {
            cookBeef(beef);
        })
        .then((cookedBeef) => {
            getBuns('cipolla')
                .then((buns) => {
                    putBeefBetweenBuns(buns, cookedBeef)
                });
        })
        .then((burger) => {
            serve(burger);
        });
};

const burger = makeBurger(); // Prepara l'hamburger
