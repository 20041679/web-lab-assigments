let titles = 'Poor Things, Oppenheimer  ,   Killers of the flower moon ,Dune: Part Two';
let stripped = titles
    .split(/\s*,\s*/);
let acronyms = stripped
    .map((v) => v
        .split(/\s+/)
        .map((word) => word[0].toUpperCase())
        .join('')
    );

console.log('titles: ' + stripped);
console.log('acronyms: ' + acronyms);
