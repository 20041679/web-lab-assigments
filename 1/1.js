let scores = [92, 82, 32, 70, 33, 66, 15, 25, 13];

let max = Math.max(...scores);
let min = Math.min(...scores);

let cleaned = scores.slice().filter((v) => v != max && v != min);

cleaned.sort();
let half = Math.ceil(cleaned.length / 2);
let lesserHalf = cleaned.slice(0, half);
let greaterHalf = cleaned.slice(half);

function average(array) {
    return array.reduce((sum, v) => sum + v, 0) / array.length
}

cleaned.push(average(lesserHalf), average(greaterHalf));

console.log('before cleaning: ' + scores);
console.log('average: ' + Math.round(average(scores)));
console.log('after cleaning: ' + cleaned);
console.log('average: ' + Math.round(average(cleaned)));
